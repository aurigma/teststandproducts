var ɵ1 = function (e) { var t = {}; function n(r) { if (t[r])
    return t[r].exports; var o = t[r] = { i: r, l: !1, exports: {} }; return e[r].call(o.exports, o, o.exports, n), o.l = !0, o.exports; } return n.m = e, n.c = t, n.d = function (e, t, r) { n.o(e, t) || Object.defineProperty(e, t, { enumerable: !0, get: r }); }, n.r = function (e) { "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, { value: "Module" }), Object.defineProperty(e, "__esModule", { value: !0 }); }, n.t = function (e, t) { if (1 & t && (e = n(e)), 8 & t)
    return e; if (4 & t && "object" == typeof e && e && e.__esModule)
    return e; var r = Object.create(null); if (n.r(r), Object.defineProperty(r, "default", { enumerable: !0, value: e }), 2 & t && "string" != typeof e)
    for (var o in e)
        n.d(r, o, function (t) { return e[t]; }.bind(null, o)); return r; }, n.n = function (e) { var t = e && e.__esModule ? function () { return e.default; } : function () { return e; }; return n.d(t, "a", t), t; }, n.o = function (e, t) { return Object.prototype.hasOwnProperty.call(e, t); }, n.p = "", n(n.s = 0); }, ɵ0 = function (e, t, n) {
    "use strict";
    function r(e, t, n) {
        if (n === void 0) { n = !1; }
        return new Promise(function (r, o) { var u = "resolve_callback_" + Math.round(1e5 * Math.random()); window[u] = (function () {
            var t = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                t[_i] = arguments[_i];
            }
            delete window[u], document.head.removeChild(c);
            var n = Array.from(t);
            var o = e.split(",");
            o = o.map(function (e) { return e.trim(); });
            var i = n.reduce(function (e, t, n) { return (e[o[n]] = t, e); }, {});
            r(i);
        }); var c = document.createElement("script"); c.type = "module", c.onerror = (function () { console.error("Failer load script from " + t), o(); }); c.text = n ? "import " + e + " from '" + t + "'; " + u + "(" + e + ".default || " + e + " );" : "import { " + e + " } from '" + t + "'; " + u + "(" + e + ");", document.head.appendChild(c); });
    }
    function o(e, t) { return r(e, t); }
    function u(e, t) { return r(e, t, !0); }
    function c(e, t) {
        if (t === void 0) { t = {}; }
        return new Promise(function (n, r) { var o = "jsonp_callback_" + Math.round(1e5 * Math.random()); window[o] = (function (e) { delete window[o], document.body.removeChild(u), n(JSON.parse(e)); }), t && Object.keys(t).forEach(function (n) { e += (e.indexOf("?") >= 0 ? "&" : "?") + n + "=" + t[n]; }); var u = document.createElement("script"); u.src = e + (e.indexOf("?") >= 0 ? "&" : "?") + "callback=" + o, u.onerror = (function (t) { console.error("Failed to load json from " + e), console.error(t), r(t); }), document.body.appendChild(u); });
    }
    function i(e) {
        var _this = this;
        return new Promise(function (t, n) { var r = new XMLHttpRequest; r.onload = (function () { 200 === _this.status ? t(JSON.parse(_this.response)) : 404 !== _this.status && 500 !== _this.status || n(_this.response); }), r.open("get", e, !0), r.send(); });
    }
    n.r(t), n.d(t, "dynamicImport", function () { return o; }), n.d(t, "dynamicImportDefault", function () { return u; }), n.d(t, "loadJsonJsonp", function () { return c; }), n.d(t, "loadJson", function () { return i; });
};
var moduleLoader = ɵ1([ɵ0]);
export default moduleLoader;
export { ɵ1, ɵ0 };
