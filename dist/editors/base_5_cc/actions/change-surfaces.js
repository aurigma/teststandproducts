export default function(options, changedOptionName, args) {
    /**
     * arguments
     *   - surfaces = associated object where keys are the option names and value is a surface definition. E.g.
    {
		"Option1": {printAreas:[{designFile: "file1"}]},
				"Option2": [{printAreas:[{designFile: "file2_side1"}]}, {printAreas:[{designFile: "file2_side2"}]}]
		}
     */

    if (!args) {
        throw "The set-surfaces action requires arguments which specify what option name corresponds to which surface definition!";
    }

    var surfaces = args[options[changedOptionName]];
    if (!surfaces) {
        console.warn("No surface definition for '" + changedOptionName + "'='" + options[changedOptionName] + "' specified in the condition.");
        return null;
    } else {
        if (!Array.isArray(surfaces)) {
            surfaces = [surfaces];
        }
    }

    var self = this;
    switch (this.is) {
        case "au-editor":
            self = this.getEditorInstance();
            break;
        case "au-canvas":
            self = this;
            break;
        default:
            throw "Cannot use this action from " + this.is;
    }
    return self.customersCanvas.then(function(editor) {
        return editor.getProduct().then(function(product) {
            var adds = [];
            var initialLength = product.surfaces.length;
            var newLength = 0;
            var previousAdd = product.addSurface(surfaces[0]);
            var j = 1;
            adds.push(previousAdd);
            while (j < surfaces.length) {
                previousAdd = previousAdd.then(function(product) {
                    return product.addSurface(surfaces[this]);
                }.bind(j));
                j++;
                adds.push(previousAdd);
            }

            return Promise.all(adds).then(function(products) {
                var deletes = [];
                var product = products[0];
                var maxTotalLength = product.surfaces.length;
                products.forEach(function(p) {
                    if (p.surfaces.length > maxTotalLength) {
                        maxTotalLength = p.surfaces.length;
                        product = p;
                    }
                });
                newLength = maxTotalLength - initialLength;
                return product.switchTo(product.surfaces[initialLength]).then(function(product) {
                    var previousDelete = product.removeSurface(product.surfaces[0]);
                    var i = 1;
                    deletes.push(previousDelete);
                    while (i < initialLength) {
                        previousDelete = previousDelete.then(function(product) {
                            // after each remove, the number of surfaces is reduced, so we should not take surface[i] here!
                            return product.removeSurface(product.surfaces[0]);
                        });
                        i++;
                        deletes.push(previousDelete);
                    }
                    return Promise.all(deletes).then(function(products) {
                        var product = products[0];
                        products.forEach(function(p) {
                            if (p.surfaces.length == newLength) {
                                product = p;
                            }
                        });
                        if (product.surfaces.length > 1) {
                            return product.switchTo(product.surfaces[1]).then(function(product) {
                                return product.switchTo(product.surfaces[0])
                            });
                        }
                    }).then(function() {
                        var updateFunc = function() {
                            spEditor.productHandler.getAllItems().forEach(function(x) {
                                x.correspondingVObject.update();
                            });
                        };
                        return editor.eval(updateFunc);
                        // return self.getReady();
                    });
                });
            });
        });
    });
};