export default function(options, changedOptionName, args) {
    /**
     * arguments
     *   - colorTheme = string, refer OptionName through {option}
     */

    var colorTheme = args.colorTheme;

    var self = this;
    switch (this.is) {
        case "au-editor":
            self = this.getEditorInstance();
            break;
        case "au-canvas":
            self = this;
            break;
        default:
            throw "Cannot use this action from " + this.is;
    }

    self.customersCanvas.then(function(editor) {
        editor.applyProductTheme(colorTheme);
        editor.eval("spEditor.productHandler.renderCurrentSurface();");
    });
};