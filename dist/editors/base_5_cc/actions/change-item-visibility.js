export default function(options, changedOptionName, args) {
    var toggleVisibility = function(args) {
        var product = spEditor.model.product;

        product.getSurfaces().forEach(function(surface) {

            var layer = surface.getPrintAreas()[0].getContainers()[1];
            args.criteria.forEach(function(criterion) {
                layer.getItems().forEach(function(item) {
                    switch (criterion.match) {
                        case "exact":
                            if (item.name.toLowerCase() == criterion.name.toLowerCase()) {
                                item.visualizationPermissions.noPrint = criterion.noPrint;
                                item.visualizationPermissions.noShow = criterion.noShow;
                            }
                            break;
                        case "partial":
                            if (item.name.toLowerCase().indexOf(criterion.name.toLowerCase()) > -1) {
                                item.visualizationPermissions.noPrint = criterion.noPrint;
                                item.visualizationPermissions.noShow = criterion.noShow;
                            }
                            break;
                        default:
                            throw "Unexpected match: " + criterion.match
                    }
                });
            });
        });
    };


    var self = this;
    switch (this.is) {
        case "au-editor":
            self = this.getEditorInstance();
            break;
        case "au-canvas":
            self = this;
            break;
        default:
            throw "Cannot use this action from " + this.is;
    }

    return self.__methodExecuter(toggleVisibility, args).then(function() {
        // Force refresh on a current page
        self.customersCanvas.then(function(cc) {
            return cc.eval(function() {
                return spEditor._canvas.waitWebServiceCalls().then(function() {
                    return spEditor.productHandler.updateItems(spEditor.productHandler.getAllItems());
                })
            });
        });

    });

};