export default function(options, changedOptionName, args) {

    var self = this;
    switch (this.is) {
        case "au-editor":
            self = this.getEditorInstance();
            break;
        case "au-canvas":
            self = this;
            break;
        default:
            throw "Cannot use this action from " + this.is;
    }

    var aligner = function(x, y) {
        var areas = spEditor.model.product.getCurrentSurface().getPrintAreas();
        areas.forEach(function(area) {
            area.bounds.Top = y;
            area.bounds.Left = x;
        });
        spEditor.productHandler.updateView();
    };

    return self.__methodExecuter(aligner, args.location.X, args.location.Y).then(function() {
        return self.getReady();
    }.bind(this));
};