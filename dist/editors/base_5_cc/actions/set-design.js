export default function(options, changedOptionName, args) {
    /**
     * arguments
     *   - surface = int | [int] | ( null | 'current')
     *   - areas = IPrintAreaTemplate | IPrintAreaDefinition or their array, refer OptionName through {option}
     *   - options = object { preserveUserChanges: true|false, updateRevertData: true|false }
     */

    if (!args || !args.areas) {
        throw "The set-design action requires an argument 'areas' holding a print area definition!";
    }

    var isSimpleSurface = typeof args.surface === 'undefined' || args.surface === 'current';

    if ((Array.isArray(args.areas) && !Array.isArray(args.surface) && !isSimpleSurface) ||
        (!Array.isArray(args.areas) && Array.isArray(args.surface) && !isSimpleSurface)) {
        throw "The set-design action requires an argument 'areas' and 'surface' shoud be both array or simple object!";
    }

    if (Array.isArray(args.areas) && Array.isArray(args.surface) &&
        !isSimpleSurface && args.areas.length != args.surface.length) {
        throw "The set-design action requires an argument 'areas' and 'surface' shoud be have similar length!";
    }

    var areas = Array.isArray(args.areas) ?
        args.areas.map(function(area, index) {
            return { area: area, surface: Array.isArray(args.surface) ? args.surface[index] : args.surface }
        })
        : [{ area: args.areas, surface: Array.isArray(args.surface) ? args.surface[index] : args.surface }];

    var setPrintArea = function(aucanvas, areaDef, options, surfaceIndex) {
        return aucanvas.customersCanvas
            .then(function(editor) {
                return editor.getProduct().then(function(product) {
                    var surface;
                    if (typeof (surfaceIndex) === 'undefined' || surfaceIndex === 'current') {
                        surface = product.currentSurface;
                    } else {
                        surface = product.surfaces[surfaceIndex];
                    }

                    return surface.setPrintAreas(areaDef, options);
                });
            })
            .catch(function(err) {
                console.log(err);
                throw "Failed to call setDesignTo";
            });
    };

    var self = this;
    switch (this.is) {
        case "au-editor":
            self = this.getEditorInstance();
            break;
        case "au-canvas":
            self = this;
            break;
        default:
            throw "Cannot use this action from " + this.is;
    }

    return new Promise(function(resolve) {
        var pendings = areas.map(function(a, i) {
            return setPrintArea(self, a.area, args.options, a.surface);
        });
        return Promise.all(pendings).then(resolve);
    });
};