export default function(options, changedOptionName, args) {
    if (typeof (args.count) === "undefined") {
        throw "You should specify count in the repeat-surface settings!";
    }
    var self = this;


    // taken from https://stackoverflow.com/a/38658925/4173445
    var num = "zero one two three four five six seven eight nine ten eleven twelve thirteen fourteen fifteen sixteen seventeen eighteen nineteen".split(" ");
    var tens = "twenty thirty forty fifty sixty seventy eighty ninety".split(" ");

    function number2words(n) {
        if (n < 20) return num[n];
        var digit = n % 10;
        if (n < 100) return tens[~~(n / 10) - 2] + (digit ? "-" + num[digit] : "");
        if (n < 1000) return num[~~(n / 100)] + " hundred" + (n % 100 == 0 ? "" : " " + number2words(n % 100));
        return number2words(~~(n / 1000)) + " thousand" + (n % 1000 != 0 ? " " + number2words(n % 1000) : "");
    }

    // taken from https://stackoverflow.com/a/4878800/4173445
    function toTitleCase(str) {
        return str.replace(/\.*\S*/g, function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });
    }

    // -------

    // If productDefinition is string, it means that we are trying to load it from a state.
    // It means that there is no need to repeat surfaces (although it should be handled on a higher level!)
    // Task 1553: Отсекать onLoad при загрузке из стейта
    if (typeof (self.config.productDefinition) === "string") {
        return;
    }

    return this.getEditorInstance().customersCanvas.then(function(cc) {
        return cc.getProduct().then(function(prod) {
            var repeatedSurfaces = [];
            for (var i = 0; i < args.count - 1; i++) {
                repeatedSurfaces = repeatedSurfaces.concat(self.config.productDefinition.surfaces);
            }

            if (repeatedSurfaces.length === 0) {
                return new Promise(function(resolve) {
                    resolve();
                });
            }

            return prod.addSurfaces(repeatedSurfaces, 0).then(function(prod) {
                return self.getEditorInstance().getReady().then(function() {
                    var promises = [];
                    if (!!args.variableLayerName) {
                        prod.surfaces.forEach(function(surface, index) {

                            var text = args.variableText.toString().replace("{number}", index + 1);
                            text = text.replace("{words}", args.capitalize ? toTitleCase(number2words(index + 1)) : number2words(index + 1));

                            promises.push(self.getEditorInstance().updateText(args.variableLayerName, text, false, index));
                        });
                    }

                    // Workaround for
                    // Bug 1552: addSurfaces ломает SimpleOnly
                    promises.push(self.getEditorInstance().customersCanvas.then(function(cc) {
                        cc.eval(function(mode) {
                            spEditor.productHandler.setSimpleMode(mode != "Advanced");
                        }, self.config.editorConfig.initialMode)
                    }));

                    return Promise.all(promises);
                });
            });
        });
    });
};