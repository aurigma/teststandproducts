export default function(options, changedOptionName, args) {
    /**
     * arguments
     *   - width = may refer OptionName through {option}
     *   - height = may refer OptionName through {option}
     */

    if (!args || !args.width || !args.height) {
        throw "The resize-product action requires an argument 'width' and 'height'!";
    }

    var eval = function(aucanvas, width, height, func) {
        return aucanvas.customersCanvas
            .then(function(editor) {
                return editor.eval(func, width, height);
            })
            .catch(function(err) {
                console.log(err);
                throw "Failed to call eval";
            });
    };

    var resizeBehindIframe = function(width, height) {
        // ================== START HELPER ================================
        var build = {
            rect: function(x, y, width, height) {
                return new Aurigma.GraphicsMill.AjaxControls.VectorObjects.Math.RectangleF(x, y, width, height);
            },
            point: function(x, y) {
                return new Aurigma.GraphicsMill.AjaxControls.VectorObjects.Math.PointF(x, y);
            },

            // Build a surface/page of the specified size which contains a single
            // print area with the same size as the page
            page: function(width, height) {
                var area = new CustomersCanvas.Model.PrintArea(build.rect(0, 0, width, height));
                area.hiResOutput = new CustomersCanvas.Model.PrintArea.HiResOutputSettings();

                // Prepare background
                var bglayer = new CustomersCanvas.Model.PrintAreaContainer();

                var mainlayer = new CustomersCanvas.Model.PrintAreaContainer();
                mainlayer.name = "Main";
                area.setContainers([bglayer, mainlayer]);

                var result = new CustomersCanvas.Model.Surface(width, height, new CustomersCanvas.Model.SurfaceMockup([], []), [area]);
                result.proofImage = new CustomersCanvas.Model.Surface.ProofImageSettings();
                return result;
            },
            // Build a product with a single page (surface) and single print area
            // and the print area will contain the background and item layers
            product: function(width, height) {
                return new CustomersCanvas.Model.Product([build.page(width, height)]);
            }
        };
        // ==================== END HELPER ===========================

        // START CODE HERE
        var newproduct = build.product(width, height);
        var newlayer = newproduct.getSurfaces()[0].getPrintAreas()[0].getContainers()[1];
        var newitems = newlayer.getItems();
        var bounds = new Aurigma.GraphicsMill.AjaxControls.VectorObjects.Math.RectangleF(0, 0, width, height);
        var newplaceholder = new CustomersCanvas.Model.PlaceholderItem(bounds);

        var oldproduct = spEditor.model.product;
        var olditems = oldproduct.getCurrentSurface().getPrintAreas()[0].getContainers()[1].getItems();
        olditems.some(function(item) {
            if (item.type == "PlaceholderItem" && !!item.content) {
                newplaceholder = item.clone();
                newplaceholder.setTransformedRectangle(Aurigma.GraphicsMill.AjaxControls.VectorObjects.Math.RotatedRectangleF.fromRectangleF(bounds));
                return true;
            } else {
                return false;
            }
        });
        if (newplaceholder) {
            newitems.push(newplaceholder);
            newlayer.setItems(newitems);
        }
        ;
        spEditor.productHandler.setProduct(newproduct);
        spEditor.productHandler.updateView();
    };

    var self = this;
    switch (this.is) {
        case "au-editor":
            self = this.getEditorInstance();
            break;
        case "au-canvas":
            self = this;
            break;
        default:
            throw "Cannot use this action from " + this.is;
    }

    return eval(self, parseInt(args.width), parseInt(args.height), resizeBehindIframe);

};