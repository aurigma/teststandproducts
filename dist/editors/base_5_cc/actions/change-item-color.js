export default function(options, changedOptionName, args) {
    /*
        You may use it as a boilerplate action.

        The context here (this) is an instance of <au-canvas>. To get a canvas instance, use
        a code like this:

        return this.customersCanvas.then(function(cc){
            ... (cc will hold an instance of Customer's Canvas Editor from iframeApi.js)
        })

        You may use these arguments:

        - options - all options (named array)
        - changedOptionName - name of a currently modified option
        - args - arguments as they are defined in the condition, after pre-processing ({OptionName}
                are replaced by the actual values of the appropriate option)

        It may return a Promise, in this case, it should be possible to
        lock interface while this action works.
    */
    if (!args || !args.items) {
        throw "The change-item-color action require an argument 'items' which should contain a list of item names you want to change color at."
    }

    var self = this;
    switch (this.is) {
        case "au-editor":
            self = this.getEditorInstance();
            break;
        case "au-canvas":
            self = this;
            break;
        default:
            throw "Cannot use this action from " + this.is;
    }

    self.updateItemsColor(args.items, args.color || options[changedOptionName], args.surface, true).then(function() {
        // Force refresh on a current page
        self.customersCanvas.then(function(cc) {
            return cc.eval(function() {
                return spEditor._canvas.waitWebServiceCalls().then(function() {
                    return spEditor.productHandler.updateItems(spEditor.productHandler.getAllItems());
                })
            });
        });

    });
};