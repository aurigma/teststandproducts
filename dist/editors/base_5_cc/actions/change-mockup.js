export default function(options, changedOptionName, args) {
    /**
     * arguments
     *   - surface =  ( null | 'current') | int | [int]
     *   - mockup = string or object {up\down}, refer OptionName through {option}
     *   - previewMockups = array [objects {up\down} or strings]
     */

    if (!args || (!args.mockup && !args.previewMockups)) {
        throw "The change-mockup action requires an argument 'mockup' or 'previewMockups' holding a mockup definition!";
    }

    var self = this;
    switch (this.is) {
        case "au-editor":
            self = this.getEditorInstance();
            break;
        case "au-canvas":
            self = this;
            break;
        default:
            throw "Cannot use this action from " + this.is;
    }

    var previewMockups = args.previewMockups;
    var mockup = args.mockup || {};

    var prepareArgsPromise = null;
    if (args.surface == "all") {
        args.surface = [];
        prepareArgsPromise = self.customersCanvas.then(function(cc) {
            return cc.getProduct();
        }).then(function(product) {
            product.surfaces.forEach(function(cur, index, arr) {
                args.surface.push(index);
            });
            return true;
        });
    } else {
        prepareArgsPromise = new Promise(function(resolve) {
            resolve()
        });
    }
    return prepareArgsPromise.then(function() {
        var result = null;
        if (typeof args.surface === 'undefined' || args.surface === null || args.surface === 'current') {
            result = self.setMockupTo(undefined, mockup, previewMockups);
        } else if (typeof args.surface === 'number') {
            result = self.setMockupTo(args.surface, mockup, previewMockups);
        } else if (Array.isArray(args.surface)) {
            result = Promise.all(args.surface.map(function(surfaceIndex) {
                return self.setMockupTo(surfaceIndex, mockup, previewMockups);
            }, self));
        }
        return result.then(function() {
            return self.getReady();
        }.bind(self));
    }.bind(self));
};