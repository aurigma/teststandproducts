export default function(options, changedOptionName, args) {
    /*
        You may use it as a boilerplate action.

        The context here (this) is an instance of <au-canvas>. To get a canvas instance, use
        a code like this:

        return this.customersCanvas.then(function(cc){
            ... (cc will hold an instance of Customer's Canvas Editor from iframeApi.js)
        })

        You may use these arguments:

        - options - all options (named array)
        - changedOptionName - name of a currently modified option
        - args - arguments as they are defined in the condition, after pre-processing ({OptionName}
                are replaced by the actual values of the appropriate option)

        It may return a Promise, in this case, it should be possible to
        lock interface while this action works.
    */

    var self = this;
    switch (this.is) {
        case "au-editor":
            self = this.getEditorInstance();
            break;
        case "au-canvas":
            self = this;
            break;
        default:
            throw "Cannot use this action from " + this.is;
    }

};