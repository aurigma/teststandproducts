/**
 * canvas image multiply filter
#cvContent::before {
    content: "";
    display: block;
    width: inherit;
    height: inherit;
    background: url(https://images.pexels.com/photos/36764/marguerite-daisy-beautiful-beauty.jpg);
    background-size: cover;
    z-index: 2;
    position: absolute;
    mix-blend-mode: multiply;
}
 * */
export default function(options, changedOptionName, args) {
    var excludeBrowsers = args.excludeBrowsers instanceof Array ? args.excludeBrowsers : [];
    var browser = (function(agent) {
        switch (true) {
            case agent.indexOf("edge") > -1:
                return "edge";
            case agent.indexOf("opr") > -1 && !!window.opr:
                return "opera";
            case agent.indexOf("chrome") > -1 && !!window.chrome:
                return "chrome";
            case agent.indexOf("trident") > -1:
                return "ie";
            case agent.indexOf("firefox") > -1:
                return "firefox";
            case agent.indexOf("safari") > -1:
                return "safari";
            default:
                return "other";
        }
    })(window.navigator.userAgent.toLowerCase());

    var stop = excludeBrowsers.join("|").indexOf(browser) > 0;
    if (stop) {
        return Promise.resolve(false);
    }

    var aligner = function(css, styleid) {
        styleid = "au-action-custom-style-" + (styleid || "");
        var style = document.querySelector("#" + styleid);
        if (!style) {
            style = document.createElement("style");
            style.id = styleid;
            document.head.appendChild(style);
        }
        style.innerHTML = css || "";

    };

    var self = this;
    switch (this.is) {
        case "au-editor":
            self = this.getEditorInstance();
            break;
        case "au-canvas":
            self = this;
            break;
        default:
            throw "Cannot use this action from " + this.is;
    }

    return self.__methodExecuter(aligner, args.css, args.styleid);
};