export default function(options, changedOptionName, args) {

    if (!args) {
        throw "The change-item-color action require an arguments which should contain new default item config."
    }

    var self = this;
    switch (this.is) {
        case "au-editor":
            self = this.getEditorInstance();
            break;
        case "au-canvas":
            self = this;
            break;
        default:
            throw "Cannot use this action from " + this.is;
    }

    return self.customersCanvas.then(function(cc) {
        return cc.eval(function(config) {

            if (config['text']) {
                spEditor.configuration.defaultItemsConfig.text = config['text'];
            }
            ;
            if (config['boundedText']) {
                spEditor.configuration.defaultItemsConfig.boundedText = config['boundedText'];
            }
            ;
            if (config['image']) {
                spEditor.configuration.defaultItemsConfig.image = config['image'];
            }
            ;
            if (config['shape']) {
                spEditor.configuration.defaultItemsConfig.shape = config['shape'];
            }
            ;
        }, args);
    });

};