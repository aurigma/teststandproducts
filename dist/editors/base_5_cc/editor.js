window.auEditor = window.auEditor || {};
window.auEditor.actions = {};
window.auEditor.VERSION = "version: '0.0.1'";
window.auEditor.events = {
    "auPreloaderChangePrimary": "auPreloaderChangePrimary",
    "auPreloaderChangeSecondary": "auPreloaderChangeSecondary",
    "auPreloaderReady": "auPreloaderReady",
};

Object.seal(window.auEditor.events);

window.auEditor.dispatchEvent = function(eventName, data) {
    document.dispatchEvent(new CustomEvent(eventName, {
        'detail': data
    }));
};

window.auEditor.addEventListener = function(eventName, callback) {
    document.addEventListener(eventName, callback);
};

window.auEditor.dispatchChangePreloaderEvent = function(eventName, tagName) {
    window.auEditor.addEventListener(window.auEditor.events.auPreloaderReady, function() {
        window.auEditor.dispatchEvent(eventName, tagName);
    });
};

var getImportPath = function() {
    var pathParts = [];
    try {
        //Throw an error to generate a stack trace
        throw new Error();
    } catch (e) {
        //Split the stack trace into each line
        var stackLines = e.stack.split("\n");
        var callerIndex;
        //Now walk though each line until we find a path reference
        for (var i in stackLines) {
            if (!stackLines[i].match(/http[s]?:\/\//)) {
                continue;
            }
            //We skipped all the lines with out an http so we now have a script reference
            //This one is the class constructor, the next is the getScriptPath() call
            //The one after that is the user code requesting the path info (so offset by 2)
            callerIndex = Number(i) + 2;
            break;
        }

        //Now parse the string for each section we want to return
        function getPath(index) {
            return stackLines[index].match(/((http[s]?:\/\/.+\/)([^\/]+\.js))/);
        }

        pathParts = getPath[callerIndex] || getPath(callerIndex - 1);
        return pathParts[1];
    }
}

var editorJsUrl = getImportPath();
var urlRoot = editorJsUrl.substring(0, editorJsUrl.lastIndexOf("/") + 1);
var editor = {
    _editor: null,
    driver: null,
    editorType: "base",
    version: '2.2.0',
    urlRoot: urlRoot,
    loader: {
        _appendToHead: function(element) {
            document.getElementsByTagName("head")[0].appendChild(element);
        },
        initWebComponentsReadyPromise: function() {
            if (!window.webComponentsReadyPromise) {
                window.webComponentsReadyPromise = new Promise(function(resolve) {
                    return window.WebComponents ?
                        window.setTimeout(resolve) :
                        document.addEventListener("WebComponentsReady", resolve);
                });
            }
        },
        _attachEventHandlers: function(element, resolve, reject) {
            if (element.readyState) {
                element.onreadystatechange = function() {
                    if (element.readyState == "loaded" || element.readyState == "complete") {
                        element.onreadystatechange = null;
                        if (typeof (resolve) === 'function') {
                            resolve();
                        }
                    }
                };
            } else {
                element.onload = function() {
                    if (typeof (resolve) === 'function') {
                        resolve();
                    }
                };
                element.onerror = function() {
                    if (typeof (reject) === 'function') {
                        reject(arguments);
                    }
                }
            }

        },
        loadScript: function(url, id, resolve, reject) {
            var script = document.createElement("script");
            script.id = id;
            script.type = "text/javascript";
            this._attachEventHandlers(script, resolve, reject);
            this._appendToHead(script);
            script.src = url;
        },
        loadLink: function(url, type, rel, resolve, reject) {
            var link = document.createElement("link");
            link.type = type || "";
            link.rel = rel;
            this._attachEventHandlers(link, resolve, reject);
            this._appendToHead(link);
            link.href = url;
        },
        load: function(url, resolve, reject) {
            if (/\.js$/ig.test(url)) {
                this.loadScript(url, "", resolve, reject);
            } else if (/\.css$|\.scss$|\.less$/ig.test(url)) {
                this.loadLink(url, "text/css", "stylesheet", resolve, reject);
            } else if (/\.html$|\.htm$/ig.test(url)) {
                this.loadLink(url, null, "import", resolve, reject);
            } else {
                throw "bad url: " + url;
            }
        }
    },
    _appendLoadAnimation: function(parent, text) {
        try {
            var style = [
                "display:flex",
                "align-items:center",
                "justify-content:center",
                "width:100%",
                "height:100%",
                "font-family: Helvetica Neue,Helvetica,Arial,sans-serif",
                "font-size: 14px",
                "background-color:rgba(255, 255, 255, 0.9)",
                "color:#0986b6",
                "position:absolute",
                "z-index:500"
            ];
            parent.style.position = "relative";
            parent.innerHTML = '<div style="' + style.join(";") + '"><span>' + text + '</span></div>';
            return parent.firstChild;
        } catch (e) {
            throw "Failed to attach css loader element";
        }
    },
    _toggleLoadAnimation: function(la, toggle) {
        try {
            if (toggle) {
                la.removeAttribute("hidden");
            } else {
                la.setAttribute("hidden", true);
            }
        } catch (e) {
            throw "Failed to toggle loader element";
        }
    },
    _loadJson: function(url) {
        return new Promise((resolve, reject) => {
            let request = new XMLHttpRequest();
            request.onload = function() {
                if (this.status === 200) {
                    resolve(JSON.parse(this.response));
                } else if (this.status === 404 || this.status === 500) {
                    reject(this.response);
                }
            };
            request.open("get", url, true);
            request.send();
        });
    },
    render: function(parent, product, callback) {
        var self = this;
        return new Promise(function(resolve, reject) {
            var la = self._appendLoadAnimation(parent, "Please wait ...");

            self._loadJson(self.urlRoot + "/editor.json").then(function(editorJson) {
                self.version = editorJson.version;
                window.auEditor.VERSION = editorJson.version;
                var recurseLoad = function(imports) {
                    if (imports.length == 0) {
                        done();
                    } else {
                        function concatAndResolveUrl(url, concat) {
                            return url.replace(/\/$/, "") + "/" + concat.replace(/^\//, "");
                        }

                        var imported = imports.shift();
                        if (!(eval(imported.check))) {
                            if (imported["check"] == "window.WebComponents") {
                                self.loader.initWebComponentsReadyPromise();
                            }
                            self.loader.load(concatAndResolveUrl(self.urlRoot, imported.src), function() {
                                recurseLoad(imports);
                            });
                        } else {
                            recurseLoad(imports);
                        }
                    }
                };
                var rendering = null;
                var done = function() {
                    self.loader.initWebComponentsReadyPromise();
                    window.webComponentsReadyPromise.then(function() {
                        if (window.ecommerceDriver) {
                            self.driver = window.ecommerceDriver;
                        } else if (product.owner) {
                            self.driver = product.owner;
                        }
                        if (!self.driver.settings && !self.driver.settings.customersCanvasBaseUrl) {
                            throw "Customer's Canvas URL is not configured";
                        }

                        if (rendering == true) {
                            return;
                        }
                        if (rendering == null) {
                            rendering = true;
                        }
                        var config = product.configuration || self.driver.config.config || self.driver.config;
                        var iframeApiOnLoad = function() {
                            var editor = document.createElement('au-editor');
                            editor.driver = self.driver;
                            editor.config = product.configuration || self.driver.config.config || self.driver.config;
                            editor.urlRoot = self.urlRoot;
                            editor.addEventListener("attached", function() {
                                self._toggleLoadAnimation(la, false);
                            });

                            editor.addEventListener("load", function() {
                                if (typeof callback == "function") {
                                    callback();
                                }
                                rendering = false;
                                var event = new Event('editor-loaded');
                                window.dispatchEvent(event);
                                resolve();
                            });
                            self._editor = editor;
                            parent.appendChild(editor);
                        };
                        if (document.querySelector("#CcIframeApiScript")) {
                            iframeApiOnLoad();
                        } else {
                            var iframeApiUrl = self.driver.settings.customersCanvasBaseUrl + "/Resources/"
                                + (config.designEditor !== true
                                    ? "SPEditor/Scripts/IFrame/IframeApi.js"
                                    : "Generated/IframeApi.js");

                            self.loader.loadScript(
                                iframeApiUrl,
                                "CcIframeApiScript",
                                iframeApiOnLoad,
                                function() {
                                    console.log(arguments);
                                    throw "Failed to load Customer's Canvas from " + iframeApiUrl;
                                }
                            );
                        }
                    });
                };
                if (editorJson.imports instanceof Array) {
                    recurseLoad(editorJson.imports.slice(0));
                } else {
                    done()
                }
            });
        });
    },
    setLanguage: function(language) {
        this._editor.language = language;
    }

};

export default editor;