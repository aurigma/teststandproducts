var express = require('express');
var http = require('http');
var WebSocket = require('ws');
var fs = require("fs");

var config = require("./model/Config.js")

const app = express();
const server = http.createServer(app);
const wss = new WebSocket.Server({ server });

class Message {
    constructor(event, message) {
        this.event = event;
        this.message = message;
    }
}

wss.on("connection", function (ws) {
    console.info("websocket connection open");
    try {
        ws.send(JSON.stringify(new Message("openConnection", "open")));
    }
    catch (err) {
        console.log(err);
        ws.open();
    }
    var fsTimeout;
    let obj = {};
    fs.watch(config.getConfig().pathFolder, { recursive: true }, (eventType, filename) => {
        obj[filename] = eventType + "File";

        if (!fsTimeout) {
            fsTimeout = setTimeout(function () {
                Object.keys(obj).forEach((element) => {
                    let str = JSON.stringify(element);
                    str = str.slice(str.indexOf("\\") + 2, -1);

                    try{
                        if (eventType==="rename") {
                            if (fs.existsSync(config.getConfig().pathFolder + filename)) {
                                ws.send(JSON.stringify(new Message("create", str)));
                            } else {
                                ws.send(JSON.stringify(new Message("delete", str)));
                            }
                        } else {
                            ws.send(JSON.stringify(new Message(obj[element], str)));
                        }
                    }
                    catch (err){
                        console.log("Error in socket");
                    }                  
                })
                obj = {};
                fsTimeout = null
            }, 1000)
        }

    })

    ws.on("close", function (ws) {
        //ws.destroy();
        console.log("websocket connection close");
    });
});
server.listen(config.getConfig().socketPort, () => {
    console.log(`WebSocketServer started on port ${server.address().port}`);
});