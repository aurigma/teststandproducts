var fs = require('fs-extra');
var path = require("path");
var getDirName = require('path').dirname;
var Folder = require("../model/Folder.js").Folder;

var config = require("../model/Config.js");

var folder = new Folder(config.getConfig().pathFolder, null);

var getFileList = function () {

    folder.findItems();
    return folder.getItemsJson();
};

var getFileText = function (filename) {
    var file = fs.readFileSync(config.getConfig().pathFolder + filename);
    return file;
}

function getFilesFromDir(globalDir) {
    var filetree = {};

    var walkDirectory = function (path, obj) {
        var dir = fs.readdirSync(path);
        let truePath = path.substr(globalDir.length + "product".length);
        for (var i = 0; i < dir.length; i++) {
            var name = dir[i];
            var target = path + '/' + name;

            var stats = fs.statSync(target);
            if (stats.isFile()) {
                obj[name] = truePath;
            } else if (stats.isDirectory()) {
                obj[name] = {};
                walkDirectory(target, obj[name]);
            }
        }
    };

    walkDirectory(globalDir + 'product', filetree);
    return filetree;
}

function writeFile(path, contents) {
    fs.ensureDirSync(getDirName(path));
    fs.writeFileSync(path, contents);
}

module.exports = { getFileList, getFileText, getFilesFromDir, writeFile }