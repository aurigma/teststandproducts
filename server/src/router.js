var express = require('express');
var router = express.Router();
var path = require('path')
var fs = require('fs');

var config = require("../model/Config.js")

var files = require("./files.js");

// Return a list of files of the specified fileTypes in the provided dir, 
// with the file path relative to the given dir
// dir: path of the directory you want to search the files for
// fileTypes: array of file types you are search files, ex: ['.txt', '.jpg']


//getFilesFromDir(config.getConfig().pathFolder);
// var items = files.getFileList();
var file = files.getFileText;

var getFilesFromDir = files.getFilesFromDir;

router.get('/', function (req, res) {
    res.send(getFilesFromDir(config.getConfig().pathFolder));
});

router.get("/file/*", function (req, res) {
    var item = file(req.params[0]);
    console.log("get File " + req.params[0]);
    try {
        JSON.parse(item);
        res.send(item.toString());
    }
    catch (err) {
        res.send({
            "err": "invalid JSON",
            Error: err
        })
    }
});

router.get("/editors", function(req, res){
    var editors = fs.readdirSync("./dist/editors");
    res.send(editors);
})

router.get("/config", function (req, res) {
    try {
        let item = JSON.stringify(config.getConfig());
        res.send(item);
    }
    catch (err) {
        res.send({
            "err": "emptyFile"
        })
    }
});

router.post("/config", function (req, res) {
    console.log("set new config")
    config.setConfig(req.body.json);
    res.end();
});

router.post("/upload", function (req, res) {
    str = req.body.filename.replace(/\s/g, '');
    if (fs.existsSync(config.getConfig().pathFolder + str)) {
        console.log("write in exist file "+(config.getConfig().pathFolder + str))
        fs.writeFileSync(config.getConfig().pathFolder + str, JSON.stringify(req.body.json, null, 2));
    } else {
        console.log("create new file ")
        files.writeFile(config.getConfig().pathFolder + str, JSON.stringify(req.body.json, null, 2));
    }
    res.end();
});

module.exports = router;