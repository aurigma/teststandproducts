var path = require('path');
var fs = require("fs");



class Folder {
    constructor(name, parent) {
        this.name = name;
        this.parent = parent;
        this.items = [];
        this.isFolder = true;
    }
    addFile(file) {
        this.items.push(file);
    }
    addFolder(folderName) {
        var folder = new Folder(folderName, this)
        //folder.findItems();
        this.items.push(folder);
    }
    getPath() {
        var fullpath = (!this.parent) ? this.name : path.join(this.parent.getPath(), this.name);
        return fullpath;
    }
    findItems() {
        var files = fs.readdirSync(this.getPath());
        files.forEach((file) => {
            if (fs.statSync(this.getPath() + "/" + file).isDirectory()) {
                this.addFolder(file);
            } else {
                this.addFile(file);
            }
        });
    }
    getItemsJson() {
        var cache = [];
        return JSON.stringify(this.items, function (key, value) {
            if (typeof value === 'object' && value !== null) {
                if (cache.indexOf(value) !== -1) {
                    return value.name;
                }
                cache.push(value);
            }
            return value;
        });
    }
}



module.exports = { Folder };