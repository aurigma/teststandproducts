var path = require('path')
var fs = require('fs');

class Config {
    constructor() {
        console.log("config is exists? " + fs.existsSync("./global.config.json"))
        if (fs.existsSync("./global.config.json")) {
            this.config = require("../../global.config.json");
            this.config.pathFolder = path.resolve(this.config.pathFolder) + "\\";
        } else {
            this.config = {
                serverPort: 3000,
                socketPort: 8999,
                pathFolder: "./configs/",
                customersCanvasUrl: null,
                editorName: "base-editor_1.5.5",
                defaultConfig: "base-editor/base-starter.json"
            }
        }
    }

    getConfig() {
        return this.config;
    }

    setConfig(newConfig) {
        this.config = {
            serverPort: newConfig.serverPort || 3000,
            socketPort: newConfig.socketPort || 8999,
            pathFolder: newConfig.pathFolder || "./configs/",
            customersCanvasUrl: newConfig.customersCanvasUrl || "",
            editorName: newConfig.editorName || "base-editor_1.5.5",
            defaultConfig: newConfig.defaultConfig || "base-editor/base-starter.json"
        }
        fs.writeFileSync("./global.config.json", JSON.stringify(this.config, null, 2));
    }
}

var Econfig = new Config();

module.exports = Econfig;