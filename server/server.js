var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var fs = require("fs")

var config = require("./model/Config.js")

var router = require('./src/router');
var app = express();
app.use(bodyParser({limit: '50mb'}));
app.use(bodyParser.urlencoded({ 'extended': 'false' }));

app.use(express.static(path.join(__dirname, '../dist/')));

app.use('/api', router);

require("./socket.js");// - Включение или отключение сокетов...

app.listen(config.getConfig().serverPort, function () {
    console.log(`RESTServer started on port ${config.getConfig().serverPort}`);
});