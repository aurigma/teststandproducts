# What is a test stand? 

Test Stand is a simple web app which can be used to create configs for the Customer's Canvas editors (base-editor and multistep-editor). You can use it to create quick prototypes of Customer's Canvas based application. For example: 

- Develop and quickly test PSD or InDesign templates for Customer's Canvas
- Build image generator based on PsdWebService
- Create configs for base-editor or multistep-editor

It is recommended to use it on a workstation of a designer or a product manager. The config created in the test-stand can be reused in production, if base-editor or multistep-editor is properly integrated. 

# Prerequisites 

To be able to use it, you need to do the following: 

- Install node.js version 8 or higher. Tested at the version 8.9.4.  
- Install Customer's Canvas, locally or remotely. 
- Download a latest base-editor or multistep-editor and put it to a special folder (see below)
- Create a folder for configs. Consider putting this folder under the version control system (like git, Bitbucket, SVN, etc).

*NB: If you want to modify templates in Photoshop or InDesign and immediately test it, it is highly recommended to install a local copy of Customer's Canvas.*

# How to use it

## 1. Installing Test Stand

First, install Node.js (https://nodejs.org/en/). The version marked as "Recommended For Most Users" should work. If it does not, please contact us at info@aurigma.com. 

After that you should just unzip the content of this package to some folder on your machine, for example, `C:\Users\yourprofilename\test-stand`. 

## 2. Running Test Stand

In the command prompt, navigate to the folder where you have unzipped it and run `npm start`. 

At the first launch, it will download and install the dependencies. Please be patient and don't interrupt this process.

Once all dependencies are loaded, it will run a web server, by default, at the port 3000. If this port is already using on your machine, you can reconfigure it (see further). 

To start using the application, open the browser at http://localhost:3000. 

***Important:** Don't close the command prompt while working with the Test Stand.*

If you need to stop the Test Stand, press `Ctrl+C` in the command prompt or just close it. 

## 3. Configuring Test Stand

When you run the Test Stand, you will notice that nothing meaningful appears in it. It happens because the Test Stand does not know anything about your Customer's Canvas, editors, etc. 

To configure it, you should click the Settings button in the top right corner or edit the `global.config.json` file in the root of the Test Stand folder (which is automatically created once you save the settings). If you are editing the config file, you may need to stop and start the application again.

You can modify the following parameters: 

- *Port number* and *WebSocket port number* - change it only in case of any conflicts
- *Config folder* - a folder with your configs (see below)
- *Customer's Canvas URL* - a URL of your Customer's Canvas instance (local or remote)
- *Editor name* - a name of a folder containing the editor (see below).
- *Default config* - a file name (including subpath, relatively the `config` subfolder of a folder you specify above) of a config which should be opened by default when you run the app.

## 4. Working with the Test Stand

If you configure everything properly, you will see three tabs: 

- *Product* - contains the JSON file which mocks the data model of the product in your ecommerce system. In particular, it contains Options and Attributes which can be referred in the config.
- *Config* - the JSON config for the editor itself. This is a main panel where you will modify Customer's Canvas parameters and other settings. Depending on the editor type (see below), the format of this JSON varies. 
- *Editor* - the editor preview area. You can click the **Run** button in the right top of the Test Stand and it will start running the editor with your Product and Config JSON files. 

Here are the most popular tasks you may carry out in the Test Stand.

### 4.1. Developing PSD/InDesign templates

First, you need to create a file in Photoshop and save it in the `ProductTemplates\designs` folder of your Customer's Canvas instance. Make sure that as soon as you click **Save** in Photoshop, the file is saved in this folder, so that Customer's Canvas could detect changes immediately. For example, assume that you save it in the `ProductTemplates\designs\business-card\sample.psd`. 

Now pick a config, for simplicity, you can use some simple base-editor config. You should find the section called `productDefinition`. Put the name of a file relatively the `designs` folder without extension, like this: 

``` json
{
   ...
   "productDefinition": {
       "surfaces": [{
           "printAreas": [{
               "designFile": "business-card/sample"
           }]
       }]
   }
}
```

Click the **Run** button (you may use the **Ctrl+`** shortcut) and switch to the *Editor* tab. Make sure that the design appears in the editor. 

Now make changes in Photoshop, save them, switch to the Test Stand and click **Run** again. You will notice that Customer's Canvas reloads the design with new changes applied. 

The same approach works for InDesign files as well as other design elements (like mockups, preview mockups, etc).

### 4.2. Creating new configs

Follow these steps:

1. Choose some config which comes with the Test Stand you would like to start with. 
2. Click the **Clone** button. 
3. In the dialog, choose a folder name and a file name (without extension). *NOTE: You can either choose a folder from a list or type a new one; when you click \ or /, it will suggest a subfolder.* 
4. Click **Add**. You will notice that the config appears in the tree view on the left. 
5. Click the new config name in the tree view.

Now you can start editing it. Refer documentation for the base-editor or multistep-editor config for the structure. 

# Editors

You can learn more about editors here: 

https://customerscanvas.com/docs/cc/product-editors.htm

In short, an editor is a JavaScript application which embeds Customer's Canvas. It is driven by a JSON config file which is supposed to be edited in this Test Stand. 

At this moment, there are two editors are available: 

- base-editor
- multistep-editor

They have different formats of JSON config which are described in separate documents. 

## Where do I find the editors?

To request the latest editor, please contact us at info@aurigma.com. 

## Where do I install the editors? 

Once you get an editor from us, unzip it to a folder inside `dist\editors`. 

After you add the editor to this folder, go to the Test Stand settings and choose another editor there.

***Hint**: It is recommended to add the editor version to the folder name, for example, for the base-editor of the version 1.5.5, call the folder `base-editor-1.5.5`.*

## Working with both base-editor and multistep-editor

If you are creating configs for both base-editor and multistep-editor,you may find it quite annoying to switching between different editors in the same application - configs for multistep-editor don't work in base-editor and vice versa.

In this case, it is recommended to install two versions of Test Stand. For example:

- `C:\Users\yourprofilename\test-stand\base` - put only base-editor there and configure it to work with the folder which keeps only the base-editor configs.
- `C:\Users\yourprofilename\test-stand\multistep` - put only multistep-editor there and configure it to work with the folder which keeps only the multistep-editor configs.

It is highly recommended to set different port numbers for both instances so that you could run them simultaneously (e.g. main port 3000 and WebSocket port 8999 for multistep-editor and 3001 and 8998 for base-editor).

# Configs folder

This sections gives some explanations how the configs folder is organized for better understanding. Note, normally, you don't have to manipulate files in this folder outside of the Test Stand user interface, but you may need it for the backup or version control purposes. 

As explained above, you specify the root folder for all configs in the Settings. Inside this folder, Test Stand creates two subfolders: 

- `product`
- `config`

The subfolder structure and names of JSON files are mirrored. In other words, when you see the following files in the tree view: 

    \business-cards
       |- sample.json 
    \flyers
       |- a5.json

It means that the following files are actually created: 

    \configs (the root folder specified in Settings)
        \product
            \business-cards
                |- sample.json
            \flyers
                |- a5.json
        \config
            \business-cards
                |- sample.json
            \flyers
                |- a5.json

They contain the content of the *Product* and *Config* tabs correspondingly. 

Note, if you should always have a pair of a product and config file. Otherwise, Test Stand may work incorrectly. 

# Disclaimer

At this point, Test Stand is supposed to be tool used by Aurigma staff internally. It is not free of bugs, so please use it on your own risk. Under certain circumstances, it may lose your changes or overwrite a wrong config file. So be sure to backup your configs (e.g. under git or other version control system). 

Anyway, any feedback is highly appreciated. Don't hesitate to email us at info@aurigma.com for any bug reports or suggestions. 